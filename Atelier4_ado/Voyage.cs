﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Atelier4_ado
{
    public partial class Voyage : Form
    {
        public Voyage()
        {
            InitializeComponent();
        }

        SqlConnection connection = new SqlConnection(@"data source=DESKTOP-J8FLHGE\SQLEXPRESS;database=Compagnie_Voyage;Integrated Security=true");
        SqlCommand command = new SqlCommand();
        SqlDataReader abm;
        private void button2_Click(object sender, EventArgs e)
        {
            command.Connection = connection;
            command.CommandText = "select N_Voyage,Date_Voy,Ville_Dep,Ville_Arr,Duree,Nbre_Voyageur,Places_libre,Tarif,#Id_chauffeur,#IMM_vehicule from Voyage";
            try
            {
                dataGridView1.Rows.Clear();
                connection.Open();
                abm = command.ExecuteReader();
                while (abm.Read())
                {
                    dataGridView1.Rows.Add(abm["N_voyage"], abm["Date_Voy"], abm["Ville_Dep"], abm["Ville_Arr"], abm["Duree"], abm["Nbre_Voyageur"], abm["Places_libre"], abm["Tarif"], abm["#Id_chauffeur"], abm["#IMM_Vehicule"]);
                }
            }
            catch (Exception e1)
            {
                MessageBox.Show("Erreur :" + e1.Message);
            }
            finally
            {
                abm.Close();
                connection.Close();
            }
        }

        private void Voyage_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
