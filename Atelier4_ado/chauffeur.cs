﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Atelier4_ado
{
    public partial class chauffeur : Form
    {
        public chauffeur()
        {
            InitializeComponent();
        }
        SqlConnection connection = new SqlConnection(@"data source=DESKTOP-J8FLHGE\SQLEXPRESS;database= Compagnie_Voyage;Integrated Security=true");
        SqlCommand command = new SqlCommand();
        SqlDataReader ab;
        private void button1_Click(object sender, EventArgs e)
        {
            command.Connection = connection;
            command.CommandText = "select [Id_chauffeur],Nom,Prenom,Addresse,Date_Rec,Salaire from chauffeur";
            try
            {
                dataGridView1.Rows.Clear();
                connection.Open();
                ab = command.ExecuteReader();
                while (ab.Read())
                {
                    dataGridView1.Rows.Add(ab["Id_chauffeur"], ab["Nom"], ab["Prenom"], ab["Addresse"], ab["Date_Rec"], ab["Salaire"]);
                }
            }
            catch (Exception e1)
            {
                MessageBox.Show("Erreur :" + e1.Message);
            }
            finally
            {
                ab.Close();
                connection.Close();
            }
        }

        private void chauffeur_Load(object sender, EventArgs e)
        {

        }
    }
}
