﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Atelier4_ado
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void chauffeurToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.ActiveMdiChild != null) this.ActiveMdiChild.Close();
            Form frch = new chauffeur();
            frch.MdiParent = this;
            frch.Show();

        }

        private void vehiculeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.ActiveMdiChild != null) this.ActiveMdiChild.Close();
            Form frvh = new Vehicule();
            frvh.MdiParent = this;
            frvh.Show();
        }

        private void voyageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.ActiveMdiChild != null) this.ActiveMdiChild.Close();
            Form frvy = new Voyage();
            frvy.MdiParent = this;
            frvy.Show();
        }
    }
}
