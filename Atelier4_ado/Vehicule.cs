﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Atelier4_ado
{
    public partial class Vehicule : Form
    {
        public Vehicule()
        {
            InitializeComponent();
        }

        SqlConnection connection = new SqlConnection(@"data source=DESKTOP-J8FLHGE\SQLEXPRESS;database=Compagnie_Voyage;Integrated Security=true");
        SqlCommand command = new SqlCommand();
        SqlDataReader ma;

        private void button1_Click(object sender, EventArgs e)
        {
            command.Connection = connection;
            command.CommandText = "select IMM_Vehicule,Marque,Type_veh,Date_Service from Vehicule";
            try
            {
                dataGridView1.Rows.Clear();
                connection.Open();
                ma = command.ExecuteReader();
                while (ma.Read())
                {
                    dataGridView1.Rows.Add(ma["IMM_Vehicule"], ma["Marque"], ma["Type_veh"], ma["Date_Service"]);
                }
            }
            catch (Exception e1)
            {
                MessageBox.Show("Erreur :" + e1.Message);
            }
            finally
            {
                ma.Close();
                connection.Close();
            }

        }

        private void Vehicule_Load(object sender, EventArgs e)
        {

        }
    }
}
